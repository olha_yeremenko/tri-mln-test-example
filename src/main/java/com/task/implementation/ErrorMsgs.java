package com.task.implementation;

/**
 * The type Error msgs.
 */
public class ErrorMsgs {

    /**
     * The constant SIZE_ERROR.
     */
    public static final String SIZE_ERROR = "Array size is an integer within the range [2..100,000]";
    /**
     * The constant ELEMENT_ERROR.
     */
    public static final String ELEMENT_ERROR = "Each element of array should be in range [-1000..1000]";

}
