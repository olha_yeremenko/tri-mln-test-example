package com.task.implementation;

import static com.task.implementation.ErrorMsgs.ELEMENT_ERROR;
import static com.task.implementation.ErrorMsgs.SIZE_ERROR;

import java.util.Arrays;

/**
 * The type Tape equilibrium.
 */
public class TapeEquilibrium {
    /**
     * Solution int.
     *
     * @param a the a
     * @return the int
     */
    public int getSolution(int[] a) {
        arrayValidation(a);
        int sum = 0;
        int tempSum = 0;
        for (int i : a) {
            sum += i;
        }
        int min = Integer.MAX_VALUE;
        for (int p = 0; p < a.length; p++) {
            tempSum += a[p];
            sum -= a[p];

            int diff = Math.abs(sum - tempSum);
            if (diff == 0) {
                return 0;
            }
            if (diff < min) {
                min = diff;
            }
        }
        return min;
    }

    private void arrayValidation(int[] a) {
        int n = a.length;
        if (n <= 2 || n > 100000) {
            throw new IllegalArgumentException(SIZE_ERROR);
        }
        int min = Math.abs(Arrays.stream(a).min().getAsInt());
        int max = Math.abs(Arrays.stream(a).max().getAsInt());
        if (max > 1000 || min > 1000) {
            throw new IllegalArgumentException(ELEMENT_ERROR);
        }
    }


}

