import static com.task.implementation.ErrorMsgs.ELEMENT_ERROR;
import static com.task.implementation.ErrorMsgs.SIZE_ERROR;
import static org.hamcrest.MatcherAssert.assertThat;

import com.task.implementation.TapeEquilibrium;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import java.util.Arrays;
import java.util.Collection;

/**
 * The type Tape equilibrium test negative.
 */
@RunWith(Parameterized.class)
public class TapeEquilibriumTestNegative {
    /**
     * The Tape equilibrium.
     */
    public TapeEquilibrium tapeEquilibrium;
    /**
     * The Array.
     */
    @Parameter()
    public int[] array;

    /**
     * The Error msg.
     */
    @Parameter(value = 1)
    public String errorMsg;

    /**
     * Data collection.
     *
     * @return the collection
     */
    @Parameterized.Parameters()
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{-44444, 0, -580888, 78648}, ELEMENT_ERROR},
                {new int[]{-40000, 0, -580, 8}, ELEMENT_ERROR},
                {new int[]{-575777, 0, 1, 1, -10001, 58, 7}, ELEMENT_ERROR},
                {new int[]{45}, SIZE_ERROR},
                {new int[]{4, 5}, SIZE_ERROR}

        });
    }

    /**
     * Initialize.
     */
    @Before
    public void initialize() {
        tapeEquilibrium = new TapeEquilibrium();
    }

    /**
     * Perform calculation with valid value.
     */
    @Test
    public void performCalculationWithValidValue() {
        try {
            tapeEquilibrium.getSolution(array);
            Assert.fail(String.format("Expected a RuntimeException '%s'", errorMsg));
        } catch (IllegalArgumentException e) {
            assertThat(String.format("Error '%s' should se occurred", errorMsg), e.getMessage().equals(errorMsg));
        }
    }
}
