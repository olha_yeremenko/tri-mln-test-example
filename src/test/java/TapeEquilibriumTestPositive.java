import static org.hamcrest.MatcherAssert.assertThat;

import com.task.implementation.TapeEquilibrium;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import java.util.Arrays;
import java.util.Collection;

/**
 * The type Tape equilibrium test positive.
 */
@RunWith(Parameterized.class)
public class TapeEquilibriumTestPositive {
    /**
     * The Tape equilibrium.
     */
//
    public TapeEquilibrium tapeEquilibrium;
    /**
     * The Array.
     */
    @Parameter()
    public int[] array;

    /**
     * The Expected.
     */
    @Parameter(value = 1)
    public int expected;

    /**
     * Data collection.
     *
     * @return the collection
     */
    @Parameterized.Parameters()
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{12, 13, 0, 2, -4}, 1},
                {new int[]{100, 145, 418, 157}, 330},
                {new int[]{-18, 542, -442, 545, 724, 0, 5, -786}, 406},
                {new int[]{1, 2,45}, 42},
                {new int[]{-1000, 1000,5,7}, 2},
                {new int[]{-2,0,2,5}, 5},
                {new int[]{10,0,5,5}, 0}
        });
    }

    /**
     * Initialize.
     */
    @Before
    public void initialize() {
        tapeEquilibrium = new TapeEquilibrium();
    }

    /**
     * Perform calculation with valid value.
     */
    @Test
    public void performCalculationWithValidValue() {
        int actualResult = tapeEquilibrium.getSolution(array);
        assertThat(String.format("Result should be '%s', but not '%s'", expected, actualResult),
                actualResult == expected);
    }
}
